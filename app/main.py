from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from fastapi.openapi.docs import get_swagger_ui_html
from starlette.exceptions import HTTPException as StarletteHTTPException
import os
from neomodel import config, db

from app.config import AppConfig
from app.exception_handler import default_handler

from app.db.postgres.utils import init_db

from app.api.v1.routes.api import router as api_router


def startup_event():
    # call this to load the caches for the neo4j
    db.cypher_query("CALL apoc.warmup.run(true, true, true)")


def get_application() -> FastAPI:
    init_db()

    application = FastAPI(**AppConfig.__dict__, debug=False)

    origins = [
        "http://localhost:3000",
    ]

    application.add_middleware(
        CORSMiddleware,
        allow_origins=origins,
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"],
    )

    config.DATABASE_URL = os.environ["DATABASE_URL"]
    config.AUTO_INSTALL_LABELS = True

    application.add_exception_handler(StarletteHTTPException, default_handler)
    application.add_event_handler("startup", startup_event)

    application.include_router(api_router)
    return application


app = get_application()


@app.get("/docs", include_in_schema=False)
async def custom_swagger_ui_html(req):
    root_path = req.scope.get("root_path", "").rstrip("/")
    openapi_url = root_path + app.openapi_url
    return get_swagger_ui_html(
        openapi_url=openapi_url,
        title="API",
    )
