from fastapi import Request, HTTPException

from fastapi.responses import JSONResponse


def default_handler(request: Request, exc: HTTPException):
    return JSONResponse(
        status_code=exc.status_code,
        content={"message": f"{exc.detail}", "success": False},
    )
