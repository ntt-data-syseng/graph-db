def prepareSingleNodePdfFormat(node):
    data = [
        {
            "id": node["entry"],
            "name": node["name"],
            "miscellaneous": node["miscellaneous"],
        }
        for node in node["unique_nodes"]
    ]

    return {
        "title": f'{node["definition"]} \n {node["taxonomy"]}',
        "optionalData": [
            {
                "id": node["entry"],
                "name": node["definition"],
                "miscellaneous": node["miscellaneous"],
            }
        ],
        "data": data,
    }


def prepareTaxonomyPdfFormat(nodes):
    return [prepareSingleNodePdfFormat(node) for node in nodes]


def prepareJsonPdfFormat(nodeOne, nodeTwo, common_nodes):
    # for common_nodes
    data = [
        {
            "id": node["entry"],
            "name": node["name"],
            "miscellaneous": node["miscellaneous"],
        }
        for node in common_nodes
    ]
    print(nodeOne["entry"])

    pdf_name = int(nodeOne["entry"][1:]) * int(nodeTwo["entry"][1:])

    return {
        "id": pdf_name,
        "heading": (
            f'Analysis of \n{nodeOne["definition"]} \n({nodeOne["taxonomy"]})'
            f' and \n{nodeTwo["definition"]} \n({nodeTwo["taxonomy"]})'
        ),
        "content": [
            {"title": "Common Nodes", "data": data},
        ]
        + prepareTaxonomyPdfFormat([nodeOne, nodeTwo]),
    }
