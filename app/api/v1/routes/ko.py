from fastapi import APIRouter, HTTPException

from typing import List
from app.models.core.ko import KO
from app.models.schemas.ko import (
    ListKosResponse,
    GetSingleKOResponse,
    CreateResponse,
)
from app.db.nodes.ko import KO as NodeKO

router = APIRouter(prefix="/ko", tags=["ko"])


@router.get("/all", response_model=ListKosResponse)
async def get_all_kos():

    result = NodeKO.nodes.all()

    return {"success": True, "data": [tax.__dict__ for tax in result]}


@router.get("/{entry_id}", response_model=GetSingleKOResponse)
async def get_ko(entry_id: str):

    result = NodeKO.nodes.get_or_none(entry=entry_id)

    if result is None:
        raise HTTPException(
            status_code=404, detail=f"ko with id {entry_id} does not exist"
        )

    return {"success": True, "data": result.__dict__}


@router.post("/bulkCreate")
async def create_kos(kos: List[KO]):
    data = NodeKO.create_or_update(*[ko.dict() for ko in kos])
    return {"success": True, "data": data}


@router.post("/", response_model=CreateResponse)
async def create_ko(ko: KO):

    data = ko.dict()
    NodeKO.create_or_update(data)

    return {"success": True, "data": data}
