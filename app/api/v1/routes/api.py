from fastapi import APIRouter

from app.api.v1.routes import ko, ranking, taxonomy, action

router = APIRouter(prefix="/v1")

router.include_router(ko.router, tags=["kos"])
router.include_router(taxonomy.router, tags=["taxonomy"])
router.include_router(action.router, tags=["action"])
router.include_router(ranking.router, tags=["ranking"])
