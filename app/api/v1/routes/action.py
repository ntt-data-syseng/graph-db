import io
import os
import requests

from fastapi import APIRouter, HTTPException, Query, Depends
from fastapi.responses import StreamingResponse
from neomodel import db
from sqlalchemy.orm import Session
from typing import List


from app.db.nodes.taxonomy import Taxonomy as NodeTaxonomy
from app.db.nodes.ko import KO as NodeKO
from app.models.schemas.action import AnalyseResponse, BookmarkResponse
from app.db.postgres.action import (
    add_bookmark,
    increment_analysis_counter,
    get_bookmarks,
)
from app.db.postgres.utils import get_db
from app.api.v1.utils.pdf import prepareJsonPdfFormat
from app.api.v1.validators.validate_user import validate_user_id


async def performAnalysis(current_id: str, to_id: str):
    results, _ = db.cypher_query(
        "MATCH (t1:Taxonomy {entry: $current_id})--(n:KO)--(t2:Taxonomy"
        " {entry: $to_id}) RETURN n, t1, t2;",
        {"current_id": current_id, "to_id": to_id},
    )
    common_nodes = [NodeKO.inflate(row[0]).__dict__ for row in results]

    common_ids = [current["entry"] for current in common_nodes]

    test_one = NodeTaxonomy.get_with_unique_kos(current_id, common_ids)
    test_two = NodeTaxonomy.get_with_unique_kos(to_id, common_ids)

    return (test_one, test_two, common_nodes)


router = APIRouter(prefix="/action")


@router.get("/analyse", response_model=AnalyseResponse)
async def analyse(
    tax_id: List[str] = Query(None, min_length=2), postgresDb: Session = Depends(get_db)
):

    try:
        (test_one, test_two, common_nodes) = await performAnalysis(tax_id[0], tax_id[1])
        increment_analysis_counter(postgresDb, tax_id[0], tax_id[1])
    except IndexError:
        raise HTTPException(status_code=404, detail="Taxonomy selected does not exist")

    return {
        "success": True,
        "data": {"nodes": [test_one, test_two], "common_nodes": common_nodes},
    }


@router.get("/bookmarks", response_model=BookmarkResponse)
async def getBookmarks(
    postgresDb: Session = Depends(get_db), user_id: str = Depends(validate_user_id)
):
    results = get_bookmarks(postgresDb, user_id)

    return {"success": True, "bookmarks": results}


@router.post("/bookmarks", status_code=201)
async def createBookmark(
    tax_id: List[str] = Query(None, min_length=2),
    postgresDb: Session = Depends(get_db),
    user_id: str = Depends(validate_user_id),
):
    add_result, exists = add_bookmark(postgresDb, tax_id[0], tax_id[1], user_id)
    if add_result:
        return {
            "success": True,
            "message": f"Bookmark added for {tax_id[0]} and {tax_id[1]}",
        }
    return {
        "success": False,
        "message": "You have already bookmarked this"
        if exists
        else "This analysis is not valid to be bookmarked",
    }


@router.get("/pdf")
async def getPdf(tax_id: List[str] = Query(None, min_length=2)):
    try:
        (test_one, test_two, common_nodes) = await performAnalysis(tax_id[0], tax_id[1])
    except IndexError:
        raise HTTPException(status_code=404, detail="Taxonomy selected does not exist")

    pdf_service = os.environ["PDF_SERVICE"]
    test = prepareJsonPdfFormat(test_one, test_two, common_nodes)
    print(test)
    response = requests.post(
        f"{pdf_service}/v1/pdf",
        json=test,
    )
    return StreamingResponse(io.BytesIO(response.content), media_type="application/pdf")


@router.post("/connect/{tax_id}/{ko_id}")
async def connect(tax_id: str, ko_id: str):

    taxonomy = NodeTaxonomy.nodes.get_or_none(entry=tax_id)
    ko = NodeKO.nodes.get_or_none(entry=ko_id)

    if taxonomy is None or ko is None:
        raise HTTPException(
            status_code=404, detail="The taxonomy or the ko does not exist"
        )

    taxonomy.has_ko.connect(ko)

    return {"success": True, "data": {"tax_id": tax_id, "ko_id": ko_id}}
