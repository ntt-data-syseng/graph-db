from fastapi import APIRouter, HTTPException
from typing import List

from app.models.schemas.taxonomy import (
    ListTaxonomiesResponse,
    GetSingleTaxonomyResponse,
    CreateResponse,
)
from app.models.core.taxonomy import Taxonomy


from app.db.nodes.taxonomy import Taxonomy as NodeTaxonomy

router = APIRouter(prefix="/taxonomy", tags=["taxonomy"])


@router.get("/all", response_model=ListTaxonomiesResponse)
async def get_all_taxonomies():

    result = NodeTaxonomy.nodes.all()
    return {"success": True, "data": [tax.__dict__ for tax in result]}


@router.get("/{entry_id}", response_model=GetSingleTaxonomyResponse)
async def get_taxonomy(entry_id: str):

    result = NodeTaxonomy.nodes.get_or_none(entry=entry_id)

    if result is None:
        raise HTTPException(
            status_code=404, detail=f"taxonomy with id {entry_id} does not exist"
        )

    return {"success": True, "data": result.__dict__}


@router.post("/", response_model=CreateResponse)
async def create_taxonomy(taxonomy: Taxonomy):

    data = taxonomy.dict()
    NodeTaxonomy.create_or_update(data)

    return {"success": True, "data": data}


@router.post("/bulkCreate")
async def create_taxonomies(taxonomies: List[Taxonomy]):
    data = NodeTaxonomy.create_or_update(*[taxonomy.dict() for taxonomy in taxonomies])
    return {"success": True, "data": data}
