from fastapi import APIRouter, HTTPException, Depends
from sqlalchemy.orm import Session
from sqlalchemy.exc import SQLAlchemyError

from app.db.postgres.action import get_analysis_rankings
from app.db.postgres.utils import get_db

router = APIRouter(prefix="/ranking", tags=["ranking"])


@router.get("")
async def get_rankings(db: Session = Depends(get_db)):
    try:
        test = get_analysis_rankings(db)
    except SQLAlchemyError:
        raise HTTPException(
            status_code=401, detail="There has been an error trying to get the rankings"
        )
    return {"success": True, "results": test}
