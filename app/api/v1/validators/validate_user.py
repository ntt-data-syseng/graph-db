from fastapi import Request, HTTPException


async def validate_user_id(request: Request):
    user_id = request.headers.get("user_id")
    if not user_id:
        raise HTTPException(status_code=400, detail="User ID Not Provided")
    return user_id
