from pydantic import BaseModel


class KO(BaseModel):
    entry: str
    name: str
    miscellaneous: str
