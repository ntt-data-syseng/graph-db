from pydantic import BaseModel
from typing import List


class Taxonomy(BaseModel):
    entry: str
    taxonomy: str
    definition: str
    miscellaneous: str


class SelectedTaxonomies(BaseModel):
    tax_ids: List[Taxonomy]
