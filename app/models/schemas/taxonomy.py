from typing import List
from pydantic import BaseModel

from app.models.core.ko import KO
from app.models.core.taxonomy import Taxonomy


class BaseResponse(BaseModel):
    success: bool


class CreateResponse(BaseResponse):
    data: Taxonomy


class ListTaxonomiesResponse(BaseResponse):
    data: List[Taxonomy]


class GetSingleTaxonomyResponse(BaseResponse):
    data: Taxonomy


class AnalyseResponse(BaseResponse):
    data: List[KO]
