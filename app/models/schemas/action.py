from typing import List, TypedDict
from pydantic import BaseModel
from app.models.core.ko import KO
from app.models.core.taxonomy import Taxonomy


class BaseResponse(BaseModel):
    success: bool


class TaxonomyWithUniqueNodes(Taxonomy):
    unique_nodes: List[KO]


class Analysis(BaseModel):
    nodes: List[TaxonomyWithUniqueNodes]
    common_nodes: List[KO]


class AnalyseResponse(BaseResponse):
    data: Analysis


class Bookmark(BaseModel):
    id: int
    user_id: str
    analysis_id: int
    tax_one: str
    tax_two: str

    class Config:
        orm_mode = True


class BookmarkResponse(BaseResponse):
    bookmarks: List[Bookmark]


class TaxAndKO(TypedDict):
    tax_id: str
    ko_id: str


class ConnectResponse(BaseResponse):
    data: TaxAndKO
