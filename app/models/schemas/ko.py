from typing import List
from pydantic import BaseModel
from app.models.core.ko import KO


class BaseResponse(BaseModel):
    success: bool


class CreateResponse(BaseResponse):
    data: KO


class ListKosResponse(BaseResponse):
    data: List[KO]


class GetSingleKOResponse(BaseResponse):
    data: KO
