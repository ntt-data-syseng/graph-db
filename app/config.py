import os

app_config = {
    "title": "Graph Database Backend",
    "description": "Backend that provides CRUD functionalities to interact with"
    " the taxonomies and the genome",
    "version": "1.0",
}


class AppConfig:
    title = "Graph Database Backend"
    description = "Backend that provides CRUD functionalities to interact with"
    " the taxonomies and the genome"
    version = "1.0"
    root_path = os.environ["ROOT_PATH"]
