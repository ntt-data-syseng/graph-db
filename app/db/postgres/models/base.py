from sqlalchemy import Column, DateTime, func


class BaseModel:
    created_at = Column(DateTime, server_default=func.now())
    updated_at = Column(DateTime, server_default=func.now(), server_onupdate=func.now())
