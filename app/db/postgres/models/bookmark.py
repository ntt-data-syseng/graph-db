from sqlalchemy import Column, Integer, String
from app.db.postgres.db import Base
from app.db.postgres.models.base import BaseModel


class Bookmarks(Base, BaseModel):
    __tablename__ = "bookmarks"

    id = Column(Integer, primary_key=True, index=True, autoincrement=True)
    user_id = Column(String)
    analysis_id = Column(Integer)
    tax_one = Column(String)
    tax_two = Column(String)
