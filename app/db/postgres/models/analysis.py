from sqlalchemy import Column, DateTime, Integer, String, func
from app.db.postgres.db import Base
from app.db.postgres.models.base import BaseModel


class Analysis(Base, BaseModel):
    __tablename__ = "analyses"

    id = Column(Integer, primary_key=True, index=True)
    tax_one = Column(String)
    tax_two = Column(String)
    count = Column(Integer, default=0)
