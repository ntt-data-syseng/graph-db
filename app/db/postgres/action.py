from sqlalchemy.orm import Session
from app.db.postgres.models.analysis import Analysis
from app.db.postgres.models.bookmark import Bookmarks


def increment_analysis_counter(db: Session, tax_one: str, tax_two: str):
    combined_id = int(tax_one[1:]) * int(tax_two[1:])

    current_record = (
        db.query(Analysis).filter(Analysis.id == combined_id).first()
    )

    if current_record:
        current_record.count += 1
    else:
        new = Analysis(id=combined_id, tax_one=tax_one, tax_two=tax_two, count=1)
        db.add(new)
    db.commit()
    return True


def add_bookmark(db: Session, tax_one: str, tax_two: str, user_id: str):
    combined_id = int(tax_one[1:]) * int(tax_two[1:])

    current_analysis = db.query(Analysis).filter_by(id=combined_id).first()
    if not current_analysis:
        return (False, False)

    exist_record = (
        db.query(Bookmarks)
        .filter_by(analysis_id=combined_id, user_id=user_id)
        .first()
    )

    if exist_record:
        return (False, True)

    new_history = Bookmarks(
        tax_one=tax_one, tax_two=tax_two, analysis_id=combined_id, user_id=user_id
    )
    db.add(new_history)
    db.commit()
    return (True, True)


def get_analysis_rankings(db: Session):
    return (
        db.query(Analysis).order_by(Analysis.count.desc()).limit(5).all()
    )


def get_bookmarks(db: Session, user_id: str):
    return (
        db.query(Bookmarks)
        .filter_by(user_id=user_id)
        .order_by(Bookmarks.updated_at)
        .all()
    )
