from neomodel import StructuredNode, StringProperty, Relationship, db
from typing import List
from app.db.nodes.ko import KO


class Taxonomy(StructuredNode):
    entry = StringProperty(unique_index=True, required=True)
    taxonomy = StringProperty(unique_index=True, required=True)
    definition = StringProperty(required=True)
    miscellaneous = StringProperty(required=True)
    has_ko = Relationship(".ko.KO", "HAS")

    @staticmethod
    def get_with_unique_kos(taxonomy_id: str, common_nodes: List[str]):

        results, _ = db.cypher_query(
            # have to be in one line cannot multiline this. Will query will fail
            "MATCH (t1: Taxonomy {entry: $taxonomy_id}) RETURN t1 as results UNION WITH $common_ids AS common_nodes MATCH (t1: Taxonomy {entry: $taxonomy_id})-[:HAS]-(k:KO) WHERE NOT k.entry in common_nodes RETURN k as results;",  # noqa: E501
            {"taxonomy_id": taxonomy_id, "common_ids": common_nodes},
        )
        taxonomy = results[0]
        kos = results[1:]

        taxonomy_information = Taxonomy.inflate(taxonomy[0]).__dict__
        unique_kos = []
        if len(kos) >= 1:
            unique_kos = [KO.inflate(row[0]).__dict__ for row in kos]

        return {**taxonomy_information, "unique_nodes": unique_kos}
