from neomodel import StructuredNode, StringProperty, Relationship


class KO(StructuredNode):
    entry = StringProperty(unique_index=True, required=True)
    name = StringProperty(required=True)
    miscellaneous = StringProperty(required=True)
    part_of = Relationship(".taxonomy.Taxonomy", "BELONGS")
