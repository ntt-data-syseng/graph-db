from fastapi.testclient import TestClient
import pytest
from app.main import app


@pytest.fixture()
def mock_client():
    from app.main import app

    with TestClient(app) as test_client:
        yield test_client


client = TestClient(app)


def test_get_single_ko():
    response = client.get("/v1/ko/K19254")
    assert response.status_code == 200
    assert response.json() == {
        "success": True,
        "data": {
            "entry": "K19254",
            "name": "Coronaviridae (excluding betacoronavirus) spike glycoprotein",
            "miscellaneous": "ENTRY       K19254                      KO\nSYMBOL      S\nNAME        Coronaviridae (excluding betacoronavirus) spike glycoprotein\nBRITE       KEGG Orthology (KO) [BR:ko00001]\n             09180 Brite Hierarchies\n              09185 Viral protein families\n               03200 Viral proteins\n                K19254  S; Coronaviridae (excluding betacoronavirus) spike glycoprotein\n               03210 Viral fusion proteins\n                K19254  S; Coronaviridae (excluding betacoronavirus) spike glycoprotein\n            Viral proteins [BR:ko03200]\n             +ssRNA viruses\n              Coronavirus\n               K19254  S; Coronaviridae (excluding betacoronavirus) spike glycoprotein\n            Viral fusion proteins [BR:ko03210]\n             Class I fusion proteins\n              Coronaviridae\n               K19254  S; Coronaviridae (excluding betacoronavirus) spike glycoprotein\nGENES       VG: 11266523(Sc-BatCoV-512_gp2) 11945622(S) 11945632(S) 11945643(S) 11945654(S) 11945665(S) 11945677(S) 13842716(D425_gp2) 1489741(2) 16607888(S) 18585628(CN73_gp03) 26613664(S) 26626388(AVT88_gp3) 26626797(AVU60_gp2) 26626965(AVU57_gp2) 26628034(AVU56_gp2) 26628970(AVU58_gp2) 27963644(S) 2943499(HCNV63gp2) 30522871(S) 30853692(S) 33133641(CD915_gp3) 33349973(S) 37616145(S) 37619363(s) 37627015(S) 54124711(HGI19_gp2) 54124771(S) 54124778(HGI29_gp03) 55060221(S) 55060242(S) 55060248(S) 55060266(S) 5741389(S) 6020262(S) 6020275(S) 6264443(CoVSW1_gp03) 6353553(S) 7040215(S) 7040226(S) 918758(S) 920849(S) 935184(PEDVgp2)\nREFERENCE   PMID:2345367\n  AUTHORS   Raabe T, Schelle-Prinz B, Siddell SG\n  TITLE     Nucleotide sequence of the gene encoding the spike glycoprotein of human coronavirus HCV 229E.\n  JOURNAL   J Gen Virol 71 ( Pt 5):1065-73 (1990)\n            DOI:10.1099",
        },
    }


def test_get_single_ko_fail():
    response = client.get("/v1/ko/K192511")
    assert response.status_code == 404
    assert response.json() == {
        "message": "ko with id K192511 does not exist",
        "success": False,
    }


# TODO: find a better way to test this
class ErrorResponse:
    def __init__(self):
        self.status_code = 400

    def json(self):
        return {"success": False, "message": "network error"}


def mocked_response(test):
    return ErrorResponse()


def test_get_all_ko_fail(monkeypatch):
    monkeypatch.setattr(client, "get", mocked_response)
    response = client.get("/v1/ko/all")

    assert response.json() == {"success": False, "message": "network error"}


def test_get_all_kos():
    response = client.get("/v1/ko/all")
    assert response.status_code == 200
    assert response.json() == {
        "success": True,
        "data": [
            {
                "entry": "K19254",
                "name": "Coronaviridae (excluding betacoronavirus) spike glycoprotein",
                "miscellaneous": "ENTRY       K19254                      KO\nSYMBOL      S\nNAME        Coronaviridae (excluding betacoronavirus) spike glycoprotein\nBRITE       KEGG Orthology (KO) [BR:ko00001]\n             09180 Brite Hierarchies\n              09185 Viral protein families\n               03200 Viral proteins\n                K19254  S; Coronaviridae (excluding betacoronavirus) spike glycoprotein\n               03210 Viral fusion proteins\n                K19254  S; Coronaviridae (excluding betacoronavirus) spike glycoprotein\n            Viral proteins [BR:ko03200]\n             +ssRNA viruses\n              Coronavirus\n               K19254  S; Coronaviridae (excluding betacoronavirus) spike glycoprotein\n            Viral fusion proteins [BR:ko03210]\n             Class I fusion proteins\n              Coronaviridae\n               K19254  S; Coronaviridae (excluding betacoronavirus) spike glycoprotein\nGENES       VG: 11266523(Sc-BatCoV-512_gp2) 11945622(S) 11945632(S) 11945643(S) 11945654(S) 11945665(S) 11945677(S) 13842716(D425_gp2) 1489741(2) 16607888(S) 18585628(CN73_gp03) 26613664(S) 26626388(AVT88_gp3) 26626797(AVU60_gp2) 26626965(AVU57_gp2) 26628034(AVU56_gp2) 26628970(AVU58_gp2) 27963644(S) 2943499(HCNV63gp2) 30522871(S) 30853692(S) 33133641(CD915_gp3) 33349973(S) 37616145(S) 37619363(s) 37627015(S) 54124711(HGI19_gp2) 54124771(S) 54124778(HGI29_gp03) 55060221(S) 55060242(S) 55060248(S) 55060266(S) 5741389(S) 6020262(S) 6020275(S) 6264443(CoVSW1_gp03) 6353553(S) 7040215(S) 7040226(S) 918758(S) 920849(S) 935184(PEDVgp2)\nREFERENCE   PMID:2345367\n  AUTHORS   Raabe T, Schelle-Prinz B, Siddell SG\n  TITLE     Nucleotide sequence of the gene encoding the spike glycoprotein of human coronavirus HCV 229E.\n  JOURNAL   J Gen Virol 71 ( Pt 5):1065-73 (1990)\n            DOI:10.1099",
            },
            {
                "entry": "K24149",
                "name": "SARS coronavirus replicase polyprotein 1ab",
                "miscellaneous": "ENTRY       K24149                      KO\nSYMBOL      pp1ab\nNAME        SARS coronavirus replicase polyprotein 1ab\nPATHWAY     map03230  Viral genome structure\n            map05171  Coronavirus disease - COVID-19\nBRITE       KEGG Orthology (KO) [BR:ko00001]\n             09120 Genetic Information Processing\n              09125 Information processing in viruses\n               03230 Viral genome structure\n                K24149  pp1ab; SARS coronavirus replicase polyprotein 1ab\n             09160 Human Diseases\n              09172 Infectious disease: viral\n               05171 Coronavirus disease - COVID-19\n                K24149  pp1ab; SARS coronavirus replicase polyprotein 1ab\n             09180 Brite Hierarchies\n              09185 Viral protein families\n               03200 Viral proteins\n                K24149  pp1ab; SARS coronavirus replicase polyprotein 1ab\n            Viral proteins [BR:ko03200]\n             +ssRNA viruses\n              Coronavirus\n               K24149  pp1ab; SARS coronavirus replicase polyprotein 1ab\nGENES       VG: 1489680(ORF1ab) 43740578(ORF1ab) 9714832(ORF1ab)\nREFERENCE   PMID:32015508\n  AUTHORS   Wu F, Zhao S, Yu B, Chen YM, Wang W, Song ZG, Hu Y, Tao ZW, Tian JH, Pei YY, Yuan ML, Zhang YL, Dai FH, Liu Y, Wang QM, Zheng JJ, Xu L, Holmes EC, Zhang YZ\n  TITLE     A new coronavirus associated with human respiratory disease in China.\n  JOURNAL   Nature 10.1038",
            },
            {
                "entry": "K24152",
                "name": "SARS coronavirus spike glycoprotein",
                "miscellaneous": "ENTRY       K24152                      KO\nSYMBOL      S\nNAME        SARS coronavirus spike glycoprotein\nPATHWAY     map03230  Viral genome structure\n            map05171  Coronavirus disease - COVID-19\nNETWORK     nt06136  Complement activation (viruses)\n            nt06171  Severe acute respiratory syndrome coronavirus 2 (SARS-CoV-2)\n  ELEMENT   N01307  SARS-CoV-2 S to AngII-AT1R-NOX2 signaling pathway\n            N01312  SARS-CoV-2 S to lectin pathway of complement activation\n            N01314  SARS-CoV-2 S to classical pathway of complement activation\n            N01316  SARS-CoV-2 S",
            },
            {
                "entry": "K24579",
                "name": "SARS coronavirus accessory protein 3a",
                "miscellaneous": "ENTRY       K24579                      KO\nSYMBOL      3a\nNAME        SARS coronavirus accessory protein 3a\nPATHWAY     map03230  Viral genome structure\n            map05171  Coronavirus disease - COVID-19\nNETWORK     nt06122  IFN signaling (viruses)\n            nt06171  Severe acute respiratory syndrome coronavirus 2 (SARS-CoV-2)\n  ELEMENT   N01321  SARS-CoV-2 nsp1",
            },
            {
                "entry": "K24151",
                "name": "SARS coronavirus envelope small membrane protein",
                "miscellaneous": "ENTRY       K24151                      KO\nSYMBOL      E\nNAME        SARS coronavirus envelope small membrane protein\nPATHWAY     map03230  Viral genome structure\n            map05171  Coronavirus disease - COVID-19\nBRITE       KEGG Orthology (KO) [BR:ko00001]\n             09120 Genetic Information Processing\n              09125 Information processing in viruses\n               03230 Viral genome structure\n                K24151  E; SARS coronavirus envelope small membrane protein\n             09160 Human Diseases\n              09172 Infectious disease: viral\n               05171 Coronavirus disease - COVID-19\n                K24151  E; SARS coronavirus envelope small membrane protein\n             09180 Brite Hierarchies\n              09185 Viral protein families\n               03200 Viral proteins\n                K24151  E; SARS coronavirus envelope small membrane protein\n            Viral proteins [BR:ko03200]\n             +ssRNA viruses\n              Coronavirus\n               K24151  E; SARS coronavirus envelope small membrane protein\nGENES       VG: 1489671(E) 43740570(E) 9714826(E)\nREFERENCE   PMID:12730500\n  AUTHORS   Rota PA, Oberste MS, Monroe SS, Nix WA, Campagnoli R, Icenogle JP, Penaranda S, Bankamp B, Maher K, Chen MH, Tong S, Tamin A, Lowe L, Frace M, DeRisi JL, Chen Q, Wang D, Erdman DD, Peret TC, Burns C, Ksiazek TG, Rollin PE, Sanchez A, Liffick S, Holloway B, Limor J, McCaustland K, Olsen-Rasmussen M, Fouchier R, Gunther S, Osterhaus AD, Drosten C, Pallansch MA, Anderson LJ, Bellini WJ\n  TITLE     Characterization of a novel coronavirus associated with severe acute respiratory syndrome.\n  JOURNAL   Science 300:1394-9 (2003)\n            DOI:10.1126",
            },
            {
                "entry": "K24150",
                "name": "SARS coronavirus membrane protein",
                "miscellaneous": "ENTRY       K24150                      KO\nSYMBOL      M\nNAME        SARS coronavirus membrane protein\nPATHWAY     map03230  Viral genome structure\n            map05171  Coronavirus disease - COVID-19\nNETWORK     nt06122  IFN signaling (viruses)\n            nt06171  Severe acute respiratory syndrome coronavirus 2 (SARS-CoV-2)\n  ELEMENT   N01321  SARS-CoV-2 nsp1",
            },
            {
                "entry": "K24580",
                "name": "SARS coronavirus accessory protein 6",
                "miscellaneous": "ENTRY       K24580                      KO\nSYMBOL      6\nNAME        SARS coronavirus accessory protein 6\nPATHWAY     map03230  Viral genome structure\n            map05171  Coronavirus disease - COVID-19\nNETWORK     nt06122  IFN signaling (viruses)\n            nt06133  RLR signaling (viruses)\n            nt06171  Severe acute respiratory syndrome coronavirus 2 (SARS-CoV-2)\n  ELEMENT   N01319  SARS-CoV-2 nsp6 and ORF6 to RIG-I-IRF7",
            },
            {
                "entry": "K24581",
                "name": "SARS coronavirus accessory protein 7a",
                "miscellaneous": "ENTRY       K24581                      KO\nSYMBOL      7a\nNAME        SARS coronavirus accessory protein 7a\nPATHWAY     map03230  Viral genome structure\n            map05171  Coronavirus disease - COVID-19\nNETWORK     nt06122  IFN signaling (viruses)\n            nt06171  Severe acute respiratory syndrome coronavirus 2 (SARS-CoV-2)\n  ELEMENT   N01322  SARS-CoV-2 nsp6",
            },
            {
                "entry": "K24582",
                "name": "SARS coronavirus accessory protein 7b",
                "miscellaneous": "ENTRY       K24582                      KO\nSYMBOL      7b\nNAME        SARS coronavirus accessory protein 7b\nPATHWAY     map03230  Viral genome structure\n            map05171  Coronavirus disease - COVID-19\nNETWORK     nt06122  IFN signaling (viruses)\n            nt06171  Severe acute respiratory syndrome coronavirus 2 (SARS-CoV-2)\n  ELEMENT   N01321  SARS-CoV-2 nsp1",
            },
            {
                "entry": "K24106",
                "name": "SARS coronavirus nucleocapsid protein",
                "miscellaneous": "ENTRY       K24106                      KO\nSYMBOL      N\nNAME        SARS coronavirus nucleocapsid protein\nPATHWAY     map03230  Viral genome structure\n            map05171  Coronavirus disease - COVID-19\nNETWORK     nt06136  Complement activation (viruses)\n            nt06171  Severe acute respiratory syndrome coronavirus 2 (SARS-CoV-2)\n  ELEMENT   N01316  SARS-CoV-2 S",
            },
            {
                "entry": "K24583",
                "name": "SARS coronavirus accessory protein 8",
                "miscellaneous": "ENTRY       K24583                      KO\nSYMBOL      8\nNAME        SARS coronavirus accessory protein 8\nPATHWAY     map03230  Viral genome structure\n            map05171  Coronavirus disease - COVID-19\nBRITE       KEGG Orthology (KO) [BR:ko00001]\n             09120 Genetic Information Processing\n              09125 Information processing in viruses\n               03230 Viral genome structure\n                K24583  8; SARS coronavirus accessory protein 8\n             09160 Human Diseases\n              09172 Infectious disease: viral\n               05171 Coronavirus disease - COVID-19\n                K24583  8; SARS coronavirus accessory protein 8\n             09180 Brite Hierarchies\n              09185 Viral protein families\n               03200 Viral proteins\n                K24583  8; SARS coronavirus accessory protein 8\n            Viral proteins [BR:ko03200]\n             +ssRNA viruses\n              Coronavirus\n               K24583  8; SARS coronavirus accessory protein 8\nGENES       VG: 1489677(ORF8b) 43740577(ORF8)\nREFERENCE   PMID:32015508\n  AUTHORS   Wu F, Zhao S, Yu B, Chen YM, Wang W, Song ZG, Hu Y, Tao ZW, Tian JH, Pei YY, Yuan ML, Zhang YL, Dai FH, Liu Y, Wang QM, Zheng JJ, Xu L, Holmes EC, Zhang YZ\n  TITLE     A new coronavirus associated with human respiratory disease in China.\n  JOURNAL   Nature 10.1038",
            },
            {
                "entry": "K24584",
                "name": "SARS coronavirus accessory protein 10",
                "miscellaneous": "ENTRY       K24584                      KO\nSYMBOL      10\nNAME        SARS coronavirus accessory protein 10\nPATHWAY     map03230  Viral genome structure\n            map05171  Coronavirus disease - COVID-19\nBRITE       KEGG Orthology (KO) [BR:ko00001]\n             09120 Genetic Information Processing\n              09125 Information processing in viruses\n               03230 Viral genome structure\n                K24584  10; SARS coronavirus accessory protein 10\n             09160 Human Diseases\n              09172 Infectious disease: viral\n               05171 Coronavirus disease - COVID-19\n                K24584  10; SARS coronavirus accessory protein 10\n             09180 Brite Hierarchies\n              09185 Viral protein families\n               03200 Viral proteins\n                K24584  10; SARS coronavirus accessory protein 10\n            Viral proteins [BR:ko03200]\n             +ssRNA viruses\n              Coronavirus\n               K24584  10; SARS coronavirus accessory protein 10\nGENES       VG: 43740576(ORF10)\nREFERENCE   PMID:32015508\n  AUTHORS   Wu F, Zhao S, Yu B, Chen YM, Wang W, Song ZG, Hu Y, Tao ZW, Tian JH, Pei YY, Yuan ML, Zhang YL, Dai FH, Liu Y, Wang QM, Zheng JJ, Xu L, Holmes EC, Zhang YZ\n  TITLE     A new coronavirus associated with human respiratory disease in China.\n  JOURNAL   Nature 10.1038",
            },
            {
                "entry": "K24324",
                "name": "MERS coronavirus spike glycoprotein",
                "miscellaneous": "ENTRY       K24324                      KO\nSYMBOL      S\nNAME        MERS coronavirus spike glycoprotein\nPATHWAY     map03230  Viral genome structure\nBRITE       KEGG Orthology (KO) [BR:ko00001]\n             09120 Genetic Information Processing\n              09125 Information processing in viruses\n               03230 Viral genome structure\n                K24324  S; MERS coronavirus spike glycoprotein\n             09180 Brite Hierarchies\n              09185 Viral protein families\n               03200 Viral proteins\n                K24324  S; MERS coronavirus spike glycoprotein\n               03210 Viral fusion proteins\n                K24324  S; MERS coronavirus spike glycoprotein\n            Viral proteins [BR:ko03200]\n             +ssRNA viruses\n              Coronavirus\n               K24324  S; MERS coronavirus spike glycoprotein\n            Viral fusion proteins [BR:ko03210]\n             Class I fusion proteins\n              Coronaviridae\n               K24324  S; MERS coronavirus spike glycoprotein\nGENES       VG: 14254594(S) 37616432(S) 37627011(S) 37627554(S) 4835991(S) 4836002(S)\nREFERENCE   PMID:23170002\n  AUTHORS   van Boheemen S, de Graaf M, Lauber C, Bestebroer TM, Raj VS, Zaki AM, Osterhaus AD, Haagmans BL, Gorbalenya AE, Snijder EJ, Fouchier RA\n  TITLE     Genomic characterization of a newly discovered coronavirus associated with acute respiratory distress syndrome in humans.\n  JOURNAL   MBio 3:e00473-12 (2012)\n            DOI:10.1128",
            },
            {
                "entry": "K24546",
                "name": "Betacoronavirus (excluding SARS and MERS) hemagglutinin-esterase [EC:3.1.1.53]",
                "miscellaneous": "ENTRY       K24546                      KO\nSYMBOL      HE\nNAME        Betacoronavirus (excluding SARS and MERS) hemagglutinin-esterase [EC:3.1.1.53]\nBRITE       KEGG Orthology (KO) [BR:ko00001]\n             09180 Brite Hierarchies\n              09185 Viral protein families\n               03200 Viral proteins\n                K24546  HE; Betacoronavirus (excluding SARS and MERS) hemagglutinin-esterase\n            Enzymes [BR:ko01000]\n             3. Hydrolases\n              3.1  Acting on ester bonds\n               3.1.1  Carboxylic-ester hydrolases\n                3.1.1.53  sialate O-acetylesterase\n                 K24546  HE; Betacoronavirus (excluding SARS and MERS) hemagglutinin-esterase\n            Viral proteins [BR:ko03200]\n             +ssRNA viruses\n              Coronavirus\n               K24546  HE; Betacoronavirus (excluding SARS and MERS) hemagglutinin-esterase\nDBLINKS     GO: 0001681\nGENES       VG: 11273168(HE) 11948233(HE) 22807880(HE) 2732801(F) 3200425(HE) 39105216(HE) 54124704(HE) 55060278(HE) 921684(HE)\nREFERENCE   PMID:15613317\n  AUTHORS   Woo PC, Lau SK, Chu CM, Chan KH, Tsoi HW, Huang Y, Wong BH, Poon RW, Cai JJ, Luk WK, Poon LL, Wong SS, Guan Y, Peiris JS, Yuen KY\n  TITLE     Characterization and complete genome sequence of a novel coronavirus, coronavirus HKU1, from patients with pneumonia.\n  JOURNAL   J Virol 79:884-95 (2005)\n            DOI:10.1128",
            },
            {
                "entry": "K24325",
                "name": "Betacoronavirus (excluding SARS and MERS) spike glycoprotein",
                "miscellaneous": "ENTRY       K24325                      KO\nSYMBOL      S\nNAME        Betacoronavirus (excluding SARS and MERS) spike glycoprotein\nBRITE       KEGG Orthology (KO) [BR:ko00001]\n             09180 Brite Hierarchies\n              09185 Viral protein families\n               03200 Viral proteins\n                K24325  S; Betacoronavirus (excluding SARS and MERS) spike glycoprotein\n               03210 Viral fusion proteins\n                K24325  S; Betacoronavirus (excluding SARS and MERS) spike glycoprotein\n            Viral proteins [BR:ko03200]\n             +ssRNA viruses\n              Coronavirus\n               K24325  S; Betacoronavirus (excluding SARS and MERS) spike glycoprotein\n            Viral fusion proteins [BR:ko03210]\n             Class I fusion proteins\n              Coronaviridae\n               K24325  S; Betacoronavirus (excluding SARS and MERS) spike glycoprotein\nGENES       VG: 11273169(S) 11948234(S) 1489752(S) 20522555(S) 22807881(S) 28721348(S) 3200426(S) 39105218(S) 4836012(S) 54124705(S) 55060229(S) 55060279(S) 921689(S)\nREFERENCE   PMID:8376972\n  AUTHORS   Mounir S, Talbot PJ\n  TITLE     Molecular characterization of the S protein gene of human coronavirus OC43.\n  JOURNAL   J Gen Virol 74 ( Pt 9):1981-7 (1993)\n            DOI:10.1099",
            },
        ],
    }
