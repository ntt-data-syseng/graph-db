from pytest import fixture


@fixture
def mock_requests_get(monkeypatch):
    def wrapped(client, return_value=None, side_effect=None):
        def what_to_return(*args, **kwargs):
            if side_effect:
                raise side_effect  # pragma: nocover
            return return_value

        monkeypatch.setattr(client, "get", what_to_return)

    return wrapped
