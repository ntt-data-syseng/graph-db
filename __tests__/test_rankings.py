import os
from pytest import fixture
from app.db.postgres.db import Base
from fastapi.testclient import TestClient
from app.main import app
from app.db.postgres.utils import get_db

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker


SQLALCHEMY_DATABASE_URL = os.environ["POSTGRES_URL"]

engine = create_engine(SQLALCHEMY_DATABASE_URL)
TestingSessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)


def override_get_db():
    try:
        db = TestingSessionLocal()
        yield db
    finally:
        db.close()


@fixture()
def test_db():
    Base.metadata.create_all(bind=engine)
    yield
    Base.metadata.drop_all(bind=engine)


app.dependency_overrides[get_db] = override_get_db


client = TestClient(app)


def test_get_rankings_empty(test_db):
    response = client.get("/v1/ranking")
    assert response.status_code == 200
    assert response.json() == {"results": [], "success": True}


def test_get_rankings_with_data(test_db):
    client.get("/v1/action/analyse?tax_id=T40364&tax_id=T40365")

    response = client.get("/v1/ranking")
    assert response.status_code == 200
    data = response.json()

    assert data["success"] == True
    assert len(data["results"]) == 1
    added_data = data["results"][0]
    tax_one = added_data["tax_one"]
    tax_two = added_data["tax_two"]
    count = added_data["count"]
    id = added_data["id"]

    assert tax_one == "T40364"
    assert tax_two == "T40365"
    assert id == 1629292860
    assert count == 1


def test_get_rankings_multiple(test_db):
    client.get("/v1/action/analyse?tax_id=T40364&tax_id=T40365")
    client.get("/v1/action/analyse?tax_id=T40364&tax_id=T40361")
    client.get("/v1/action/analyse?tax_id=T40364&tax_id=T40361")
    response = client.get("/v1/ranking")
    assert response.status_code == 200
    data = response.json()

    assert data["success"] == True
    assert len(data["results"]) == 2

    first_data = data["results"][0]
    second_data = data["results"][1]

    assert first_data["tax_one"] == "T40364"
    assert first_data["tax_two"] == "T40361"
    assert first_data["count"] == 2
    assert first_data["id"] == 1629131404

    assert second_data["count"] == 1
