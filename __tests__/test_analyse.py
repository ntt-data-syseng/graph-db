import os
from pytest import fixture
from app.db.postgres.models.analysis import Analysis
from app.db.postgres.db import Base
from fastapi.testclient import TestClient
from app.main import app
from app.db.postgres.utils import get_db

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker


SQLALCHEMY_DATABASE_URL = os.environ["POSTGRES_URL"]

engine = create_engine(SQLALCHEMY_DATABASE_URL)
TestingSessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)


def override_get_db():
    try:
        db = TestingSessionLocal()
        yield db
    finally:
        db.close()


@fixture()
def test_db():
    Base.metadata.create_all(bind=engine)
    yield
    Base.metadata.drop_all(bind=engine)


app.dependency_overrides[get_db] = override_get_db


client = TestClient(app)


def test_analyse_fail():
    response = client.get("/v1/action/analyse?tax_id=T40361&tax_id=T400691")
    assert response.status_code == 404
    assert response.json() == {
        "message": "Taxonomy selected does not exist",
        "success": False,
    }


def test_analyse(test_db):
    response = client.get("/v1/action/analyse?tax_id=T40364&tax_id=T40365")
    assert response.status_code == 200
    assert response.json() == {
        "success": True,
        "data": {
            "nodes": [
                {
                    "entry": "T40364",
                    "taxonomy": "TAX:11137",
                    "definition": "Human coronavirus 229E",
                    "miscellaneous": "ENTRY       T40364            Viral     Genome\nDEFINITION  Human coronavirus 229E\nTAXONOMY    TAX:11137\n  LINEAGE   Viruses; Riboviria; Orthornavirae; Pisuviricota; Pisoniviricetes; Nidovirales; Cornidovirineae; Coronaviridae; Orthocoronavirinae; Alphacoronavirus; Duvinacovirus\n  SEQUENCE  RS:NC_002645\nDISEASE     Common cold [DS:H02442]\n  HOST      Homo sapiens [GN:hsa]\nDBLINKS     Virus-HostDB: 11137\nREFERENCE   PMID:11369870\n  AUTHORS   Thiel V, Herold J, Schelle B, Siddell SG\n  TITLE     Infectious RNA transcribed in vitro from a cDNA copy of the human coronavirus genome cloned in vaccinia virus.\n  JOURNAL   J Gen Virol 82:1273-1281 (2001)\n            DOI:10.1099",
                    "unique_nodes": [],
                },
                {
                    "entry": "T40365",
                    "taxonomy": "TAX:277944",
                    "definition": "Human coronavirus NL63",
                    "miscellaneous": "ENTRY       T40365            Viral     Genome\nDEFINITION  Human coronavirus NL63\nTAXONOMY    TAX:277944\n  LINEAGE   Viruses; Riboviria; Orthornavirae; Pisuviricota; Pisoniviricetes; Nidovirales; Cornidovirineae; Coronaviridae; Orthocoronavirinae; Alphacoronavirus; Setracovirus\n  SEQUENCE  RS:NC_005831\nDISEASE     Common cold [DS:H02442]\n  HOST      Homo sapiens [GN:hsa]\nCOMMENT     Also associated with croup and Kawasaki disease [DS:H01718].\nDBLINKS     Virus-HostDB: 277944\nREFERENCE   PMID:15034574\n  AUTHORS   van der Hoek L, Pyrc K, Jebbink MF, Vermeulen-Oost W, Berkhout RJ, Wolthers KC, Wertheim-van Dillen PM, Kaandorp J, Spaargaren J, Berkhout B\n  TITLE     Identification of a new human coronavirus.\n  JOURNAL   Nat Med 10:368-73 (2004)\n            DOI:10.1038",
                    "unique_nodes": [],
                },
            ],
            "common_nodes": [
                {
                    "entry": "K19254",
                    "name": "Coronaviridae (excluding betacoronavirus) spike glycoprotein",
                    "miscellaneous": "ENTRY       K19254                      KO\nSYMBOL      S\nNAME        Coronaviridae (excluding betacoronavirus) spike glycoprotein\nBRITE       KEGG Orthology (KO) [BR:ko00001]\n             09180 Brite Hierarchies\n              09185 Viral protein families\n               03200 Viral proteins\n                K19254  S; Coronaviridae (excluding betacoronavirus) spike glycoprotein\n               03210 Viral fusion proteins\n                K19254  S; Coronaviridae (excluding betacoronavirus) spike glycoprotein\n            Viral proteins [BR:ko03200]\n             +ssRNA viruses\n              Coronavirus\n               K19254  S; Coronaviridae (excluding betacoronavirus) spike glycoprotein\n            Viral fusion proteins [BR:ko03210]\n             Class I fusion proteins\n              Coronaviridae\n               K19254  S; Coronaviridae (excluding betacoronavirus) spike glycoprotein\nGENES       VG: 11266523(Sc-BatCoV-512_gp2) 11945622(S) 11945632(S) 11945643(S) 11945654(S) 11945665(S) 11945677(S) 13842716(D425_gp2) 1489741(2) 16607888(S) 18585628(CN73_gp03) 26613664(S) 26626388(AVT88_gp3) 26626797(AVU60_gp2) 26626965(AVU57_gp2) 26628034(AVU56_gp2) 26628970(AVU58_gp2) 27963644(S) 2943499(HCNV63gp2) 30522871(S) 30853692(S) 33133641(CD915_gp3) 33349973(S) 37616145(S) 37619363(s) 37627015(S) 54124711(HGI19_gp2) 54124771(S) 54124778(HGI29_gp03) 55060221(S) 55060242(S) 55060248(S) 55060266(S) 5741389(S) 6020262(S) 6020275(S) 6264443(CoVSW1_gp03) 6353553(S) 7040215(S) 7040226(S) 918758(S) 920849(S) 935184(PEDVgp2)\nREFERENCE   PMID:2345367\n  AUTHORS   Raabe T, Schelle-Prinz B, Siddell SG\n  TITLE     Nucleotide sequence of the gene encoding the spike glycoprotein of human coronavirus HCV 229E.\n  JOURNAL   J Gen Virol 71 ( Pt 5):1065-73 (1990)\n            DOI:10.1099",
                }
            ],
        },
    }
    # Check that the counter has been incremented
    with TestingSessionLocal.begin() as session:
        id = 40364 * 40365
        result = session.query(Analysis).filter_by(id=id).first()

        assert result.__dict__["count"] == 1
        assert result.__dict__["tax_one"] == "T40364"
        assert result.__dict__["tax_two"] == "T40365"
