from fastapi.testclient import TestClient
from app.main import app

client = TestClient(app)


def test_get_single_taxonomy():
    response = client.get("/v1/taxonomy/T40364")
    assert response.status_code == 200
    assert response.json() == {
        "success": True,
        "data": {
            "entry": "T40364",
            "taxonomy": "TAX:11137",
            "definition": "Human coronavirus 229E",
            "miscellaneous": "ENTRY       T40364            Viral     Genome\nDEFINITION  Human coronavirus 229E\nTAXONOMY    TAX:11137\n  LINEAGE   Viruses; Riboviria; Orthornavirae; Pisuviricota; Pisoniviricetes; Nidovirales; Cornidovirineae; Coronaviridae; Orthocoronavirinae; Alphacoronavirus; Duvinacovirus\n  SEQUENCE  RS:NC_002645\nDISEASE     Common cold [DS:H02442]\n  HOST      Homo sapiens [GN:hsa]\nDBLINKS     Virus-HostDB: 11137\nREFERENCE   PMID:11369870\n  AUTHORS   Thiel V, Herold J, Schelle B, Siddell SG\n  TITLE     Infectious RNA transcribed in vitro from a cDNA copy of the human coronavirus genome cloned in vaccinia virus.\n  JOURNAL   J Gen Virol 82:1273-1281 (2001)\n            DOI:10.1099",
        },
    }


def test_get_single_taxonomy_fail():
    response = client.get("/v1/taxonomy/T1111")
    assert response.status_code == 404
    assert response.json() == {
        "message": "taxonomy with id T1111 does not exist",
        "success": False,
    }


# TODO: find a better way to test this
class ErrorResponse:
    def __init__(self):
        self.status_code = 400

    def json(self):
        return {"success": False, "message": "network error"}


def mocked_response(test):
    return ErrorResponse()


def test_get_all_taxonomies_fail(monkeypatch):
    monkeypatch.setattr(client, "get", mocked_response)
    response = client.get("/v1/taxonomy/all")

    assert response.json() == {"success": False, "message": "network error"}


def test_get_all_taxonomies():
    response = client.get("/v1/taxonomy/all")
    assert response.status_code == 200
    assert response.json() == {
        "success": True,
        "data": [
            {
                "entry": "T40364",
                "taxonomy": "TAX:11137",
                "definition": "Human coronavirus 229E",
                "miscellaneous": "ENTRY       T40364            Viral     Genome\nDEFINITION  Human coronavirus 229E\nTAXONOMY    TAX:11137\n  LINEAGE   Viruses; Riboviria; Orthornavirae; Pisuviricota; Pisoniviricetes; Nidovirales; Cornidovirineae; Coronaviridae; Orthocoronavirinae; Alphacoronavirus; Duvinacovirus\n  SEQUENCE  RS:NC_002645\nDISEASE     Common cold [DS:H02442]\n  HOST      Homo sapiens [GN:hsa]\nDBLINKS     Virus-HostDB: 11137\nREFERENCE   PMID:11369870\n  AUTHORS   Thiel V, Herold J, Schelle B, Siddell SG\n  TITLE     Infectious RNA transcribed in vitro from a cDNA copy of the human coronavirus genome cloned in vaccinia virus.\n  JOURNAL   J Gen Virol 82:1273-1281 (2001)\n            DOI:10.1099",
            },
            {
                "entry": "T40365",
                "taxonomy": "TAX:277944",
                "definition": "Human coronavirus NL63",
                "miscellaneous": "ENTRY       T40365            Viral     Genome\nDEFINITION  Human coronavirus NL63\nTAXONOMY    TAX:277944\n  LINEAGE   Viruses; Riboviria; Orthornavirae; Pisuviricota; Pisoniviricetes; Nidovirales; Cornidovirineae; Coronaviridae; Orthocoronavirinae; Alphacoronavirus; Setracovirus\n  SEQUENCE  RS:NC_005831\nDISEASE     Common cold [DS:H02442]\n  HOST      Homo sapiens [GN:hsa]\nCOMMENT     Also associated with croup and Kawasaki disease [DS:H01718].\nDBLINKS     Virus-HostDB: 277944\nREFERENCE   PMID:15034574\n  AUTHORS   van der Hoek L, Pyrc K, Jebbink MF, Vermeulen-Oost W, Berkhout RJ, Wolthers KC, Wertheim-van Dillen PM, Kaandorp J, Spaargaren J, Berkhout B\n  TITLE     Identification of a new human coronavirus.\n  JOURNAL   Nat Med 10:368-73 (2004)\n            DOI:10.1038",
            },
            {
                "entry": "T40376",
                "taxonomy": "TAX:864596",
                "definition": "Bat coronavirus BM48-31/BGR/2008",
                "miscellaneous": "ENTRY       T40376            Viral     Genome\nDEFINITION  Bat coronavirus BM48-31",
            },
            {
                "entry": "T40361",
                "taxonomy": "TAX:2697049",
                "definition": "SARS-CoV-2 (Severe acute respiratory syndrome coronavirus 2)",
                "miscellaneous": "ENTRY       T40361            Viral     Genome\nDEFINITION  SARS-CoV-2 (Severe acute respiratory syndrome coronavirus 2)\nTAXONOMY    TAX:2697049\n  LINEAGE   Viruses; Riboviria; Orthornavirae; Pisuviricota; Pisoniviricetes; Nidovirales; Cornidovirineae; Coronaviridae; Orthocoronavirinae; Betacoronavirus; Sarbecovirus\n  SEQUENCE  RS:NC_045512\nDISEASE     COVID-19 [DS:H02398]\n  HOST      Homo sapiens [GN:hsa]\nDBLINKS     Virus-HostDB: 2697049\nREFERENCE   PMID:32015508\n  AUTHORS   Wu F, Zhao S, Yu B, Chen YM, Wang W, Song ZG, Hu Y, Tao ZW, Tian JH, Pei YY, Yuan ML, Zhang YL, Dai FH, Liu Y, Wang QM, Zheng JJ, Xu L, Holmes EC, Zhang YZ\n  TITLE     A new coronavirus associated with human respiratory disease in China.\n  JOURNAL   Nature 10.1038",
            },
            {
                "entry": "T40090",
                "taxonomy": "TAX:1335626",
                "definition": "MERS-CoV (Middle East respiratory syndrome coronavirus)",
                "miscellaneous": "ENTRY       T40090            Viral     Genome\nDEFINITION  MERS-CoV (Middle East respiratory syndrome coronavirus)\nTAXONOMY    TAX:1335626\n  LINEAGE   Viruses; Riboviria; Orthornavirae; Pisuviricota; Pisoniviricetes; Nidovirales; Cornidovirineae; Coronaviridae; Orthocoronavirinae; Betacoronavirus; Merbecovirus\n  SEQUENCE  RS:NC_019843\nDISEASE     Middle East respiratory syndrome [DS:H01419]\n  HOST      Homo sapiens [GN:hsa]\n  RESERVOIR Camelus dromedarius (Arabian camel) [TAX:9838]\nDBLINKS     Virus-HostDB: 1335626\nREFERENCE   PMID:23170002\n  AUTHORS   van Boheemen S, de Graaf M, Lauber C, Bestebroer TM, Raj VS, Zaki AM, Osterhaus AD, Haagmans BL, Gorbalenya AE, Snijder EJ, Fouchier RA\n  TITLE     Genomic characterization of a newly discovered coronavirus associated with acute respiratory distress syndrome in humans.\n  JOURNAL   MBio 3:e00473-12 (2012)\n            DOI:10.1128",
            },
            {
                "entry": "T40362",
                "taxonomy": "TAX:31631",
                "definition": "Human coronavirus OC43",
                "miscellaneous": "ENTRY       T40362            Viral     Genome\nDEFINITION  Human coronavirus OC43\nTAXONOMY    TAX:31631\n  LINEAGE   Viruses; Riboviria; Orthornavirae; Pisuviricota; Pisoniviricetes; Nidovirales; Cornidovirineae; Coronaviridae; Orthocoronavirinae; Betacoronavirus; Embecovirus\n  SEQUENCE  RS:NC_006213\nDISEASE     Common cold [DS:H02442]\n  HOST      Homo sapiens [GN:hsa]\nDBLINKS     Virus-HostDB: 31631\nREFERENCE   PMID:15280490\n  AUTHORS   St-Jean JR, Jacomy H, Desforges M, Vabret A, Freymuth F, Talbot PJ\n  TITLE     Human respiratory coronavirus OC43: genetic stability and neuroinvasion.\n  JOURNAL   J Virol 78:8824-34 (2004)\n            DOI:10.1128",
            },
            {
                "entry": "T40363",
                "taxonomy": "TAX:290028",
                "definition": "Human coronavirus HKU1",
                "miscellaneous": "ENTRY       T40363            Viral     Genome\nDEFINITION  Human coronavirus HKU1\nTAXONOMY    TAX:290028\n  LINEAGE   Viruses; Riboviria; Orthornavirae; Pisuviricota; Pisoniviricetes; Nidovirales; Cornidovirineae; Coronaviridae; Orthocoronavirinae; Betacoronavirus; Embecovirus\n  SEQUENCE  RS:NC_006577\nDISEASE     Common cold [DS:H02442]\n  HOST      Homo sapiens [GN:hsa]\nDBLINKS     Virus-HostDB: 290028\nREFERENCE   PMID:15613317\n  AUTHORS   Woo PC, Lau SK, Chu CM, Chan KH, Tsoi HW, Huang Y, Wong BH, Poon RW, Cai JJ, Luk WK, Poon LL, Wong SS, Guan Y, Peiris JS, Yuen KY\n  TITLE     Characterization and complete genome sequence of a novel coronavirus, coronavirus HKU1, from patients with pneumonia.\n  JOURNAL   J Virol 79:884-95 (2005)\n            DOI:10.1128",
            },
            {
                "entry": "T40069",
                "taxonomy": "TAX:227984",
                "definition": "SARS-CoV (Severe acute respiratory syndrome coronavirus)",
                "miscellaneous": "ENTRY       T40069            Viral     Genome\nDEFINITION  SARS-CoV (Severe acute respiratory syndrome coronavirus)\nTAXONOMY    TAX:227984\n  LINEAGE   Viruses; Riboviria; Orthornavirae; Pisuviricota; Pisoniviricetes; Nidovirales; Cornidovirineae; Coronaviridae; Orthocoronavirinae; Betacoronavirus; Sarbecovirus; Severe acute respiratory syndrome coronavirus\n  SEQUENCE  RS:NC_004718\nDISEASE     Severe acute respiratory syndrome [DS:H00402]\n  HOST      Homo sapiens [GN:hsa]\n  RESERVOIR Paguma larvata (masked palm civet) [TAX:9675], Nyctereutes procyonoides (raccoon dog) [TAX:34880], Melogale moschata (Chinese ferret-badger) [TAX:204267], Felis catus (domestic cat) [GN:fca], Sus scrofa (pig) [GN:ssc], Chiroptera (bats) [TAX:9397]\nDBLINKS     Virus-HostDB: 227984\nREFERENCE   PMID:12730501\n  AUTHORS   Marra MA, Jones SJ, Astell CR, Holt RA, Brooks-Wilson A, Butterfield YS, Khattra J, Asano JK, Barber SA, Chan SY, Cloutier A, Coughlin SM, Freeman D, Girn N, Griffith OL, Leach SR, Mayo M, McDonald H, Montgomery SB, Pandoh PK, Petrescu AS, Robertson AG, Schein JE, Siddiqui A, Smailus DE, Stott JM, Yang GS, Plummer F, Andonov A, Artsob H, Bastien N, Bernard K, Booth TF, Bowness D, Czub M, Drebot M, Fernando L, Flick R, Garbutt M, Gray M, Grolla A, Jones S, Feldmann H, Meyers A, Kabani A, Li Y, Normand S, Stroher U, Tipples GA, Tyler S, Vogrig R, Ward D, Watson B, Brunham RC, Krajden M, Petric M, Skowronski DM, Upton C, Roper RL\n  TITLE     The Genome sequence of the SARS-associated coronavirus.\n  JOURNAL   Science 300:1399-404 (2003)\n            DOI:10.1126",
            },
        ],
    }
