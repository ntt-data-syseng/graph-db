import os
from pytest import fixture
from app.db.postgres.db import Base
from fastapi.testclient import TestClient
from app.main import app
from app.db.postgres.utils import get_db

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker


SQLALCHEMY_DATABASE_URL = os.environ["POSTGRES_URL"]

engine = create_engine(SQLALCHEMY_DATABASE_URL)
TestingSessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)


def override_get_db():
    try:
        db = TestingSessionLocal()
        yield db
    finally:
        db.close()


@fixture()
def test_db():
    Base.metadata.create_all(bind=engine)
    yield
    Base.metadata.drop_all(bind=engine)


app.dependency_overrides[get_db] = override_get_db


client = TestClient(app)


def test_get_bookmarks_no_user_id_fail(test_db):
    response = client.get("/v1/action/bookmarks")
    assert response.status_code == 400
    assert response.json() == {"message": "User ID Not Provided", "success": False}


def test_get_bookmarks_empty(test_db):
    response = client.get("/v1/action/bookmarks", headers={"user_id": "1123123"})
    assert response.status_code == 200
    assert response.json() == {"success": True, "bookmarks": []}


def test_add_bookmarks_success(test_db):
    # perform an analysis first before you can save

    client.get("/v1/action/analyse?tax_id=T40364&tax_id=T40365")

    response = client.post(
        "/v1/action/bookmarks?tax_id=T40364&tax_id=T40365",
        headers={"user_id": "1123123"},
    )
    assert response.status_code == 201
    assert response.json() == {
        "message": "Bookmark added for T40364 and T40365",
        "success": True,
    }


def test_get_bookmarks_with_result(test_db):

    client.get("/v1/action/analyse?tax_id=T40364&tax_id=T40365")

    add_response = client.post(
        "/v1/action/bookmarks?tax_id=T40364&tax_id=T40365",
        headers={"user_id": "1123123"},
    )
    assert add_response.status_code == 201
    assert add_response.json()["success"] == True

    response = client.get("/v1/action/bookmarks", headers={"user_id": "1123123"})
    assert response.status_code == 200
    assert response.json() == {
        "success": True,
        "bookmarks": [
            {
                "id": 1,
                "user_id": "1123123",
                "analysis_id": 1629292860,
                "tax_one": "T40364",
                "tax_two": "T40365",
            }
        ],
    }


def test_add_bookmarks_no_user_id_fail(test_db):

    response = client.post("/v1/action/bookmarks?tax_id=T40364&tax_id=T40365")
    assert response.status_code == 400
    assert response.json() == {"message": "User ID Not Provided", "success": False}
