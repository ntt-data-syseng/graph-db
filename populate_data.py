from typing import Dict
import requests
import csv
import sys

# Script to populate the data

baseUrl = sys.argv[1]
print(baseUrl)

def populate_data(path: str, url: str, mapper: Dict):
  with open(path, newline='\n') as csvfile:
    csv_reader = csv.reader(csvfile, delimiter=',')
    next(csv_reader)
    data_entries = []
    for row in csv_reader:

      current_data = {key:row[value] for key,value in mapper.items()}
      data_entries.append(current_data)

    result = requests.post(url, json=data_entries)
    print(result.json())


def populate_connection(path: str):
  with open(path, newline="\n") as csvfile:
    csv_reader = csv.reader(csvfile, delimiter=",")
    next(csv_reader)
    for row in csv_reader:

      result = requests.post(f"http://{baseUrl}/v1/action/connect/{row[0]}/{row[1]}") 
      print(result.json())


taxonomy_structure = {
  "entry": 0,
  "taxonomy": 1,
  "definition": 2,
  "miscellaneous": 3
}


ko_structure = {
    "entry": 0,
    "name": 1,
    "miscellaneous": 2
  }

taxonomy_url = f"http://{baseUrl}/v1/taxonomy/bulkCreate"
ko_url = f"http://{baseUrl}/v1/ko/bulkCreate"


populate_data("./cleaned_data/results/taxonomies.csv", taxonomy_url, taxonomy_structure)
populate_data("./cleaned_data/results/ko_file.csv", ko_url, ko_structure)


populate_connection("./cleaned_data/results/taxonomy_ko_relationship.csv")

