docker-compose -f docker-compose.test.yaml build --no-cache
docker-compose -f docker-compose.test.yaml up --exit-code-from test-runner
docker-compose -f docker-compose.test.yaml down