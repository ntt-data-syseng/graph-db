# GraphDB-Backend

Backend service for our application

## Description

Provides a REST api to perform crud actions with the neo4j graph databsae

## Getting Started
### Dependencies

- Docker

### Executing program

- Docker-compose up

```
Docker-compose up
```

## Authors

Contributors names and contact info

ex. Passawis Chaiyapattanapaporn

## Version History

* 0.1
    * Initial Release