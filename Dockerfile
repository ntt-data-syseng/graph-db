# 
FROM python:3.9-slim

# 
WORKDIR /code

# 
COPY ./requirements.txt /code/requirements.txt
COPY ./wait-for-it.sh /code/wait-for-it.sh
RUN chmod +x /code/wait-for-it.sh

# 
RUN pip install --no-cache-dir -r /code/requirements.txt

# 

COPY ./app /code/app

